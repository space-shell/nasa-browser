# Nasa Browser

[ :link: - Live Demo ]( https://space-shell.gitlab.io/nasa-browser )


## Installation

**Requirements**

* Node
* NPM

Install both the Node modules and the web modules with the following command:

```bash
npm i
```

## Development

> Once the web modules have been installed no further build steps are required for JavaScript development

To start the development run the following command:

```bash
npm run serve
```

To start watch and build the SASS files run the following command:

```bash
npm run watch:sass
```
