import { useEffect, useState } from 'preact/hooks'

export const usePersistedState = ( key, default_state ) =>
  {
    const [ state, set_state ] = useState( null )

    useEffect( (  ) => {
        const stored_state = JSON.parse( localStorage.getItem( key ) )

        if ( stored_state )
          set_state( stored_state )
    }, [  ] )

    useEffect( (  ) => {
      localStorage.setItem( key, JSON.stringify( state ) )
    }, [key, state])

    return [ state, set_state ]
  }

