import * as R from 'ramda'

import { html } from 'htm/preact'

import { Router, route } from 'preact-router'

import { render, Fragment } from 'preact'

import { useEffect, useState, useMemo, useRef } from 'preact/hooks'

import { useDebouncedEffect } from '../hooks/use-debounce-effect.js'

import { usePersistedState } from '../hooks/use-persistant-state.js'

// States
const PENDING = 'pending'
const ERROR = 'error'
const COMPLETE = 'complete'

// NASA Image Sizes
const SEARCH_IMAGE_LARGE = /large/i
const SEARCH_IMAGE_MEDIUM = /medium/i
const SEARCH_IMAGE_SMALL = /small/i
const SEARCH_IMAGE_THUMBNAIL = /thumb/i

// NASA Image Sizes
const SEARCH_AUDIO_HIGH = /256k.mp3/i
const SEARCH_AUDIO_MEDIUM = /128k.mp3/i
const SEARCH_AUDIO_LOW = /64k.mp3/i

// NASA Image Sizes
const NASA_IMAGE_TYPE = 'image'
const NASA_AUDIO_TYPE = 'audio'

const toggle_sibling_audio = ({ target }) =>
  {
    const audio_element = target.nextSibling

    console.log({ target, audio_element })
    
    if ( ! audio_element.tagName === 'AUDIO' )
      return

    if ( audio_element.paused )
      audio_element.play(  )
    else
      audio_element.pause(  )
  }

// http://images-assets.nasa.gov/image/NASA 60th_SEAL_BLACK_72DPI/NASA 60th_SEAL_BLACK_72DPI~thumb.jpg -> NASA 60th_SEAL_BLACK_72DPI
export const NasaTitleGenerate =
  R.pipe
    ( R.split( '/' )
    , R.last
    , R.split( '~' )
    , R.head
    )

const ErrorPage = ({  }) =>
  {
    return html`
      <div>
        <a>
          Google...
        <//>
      <//>
    `
  }

// state-loaders.js

const StateLoaderPending = ({ children, pending = true }) =>
  {
    if ( pending )
      return html`
        <state-loader-pending>
          <span/>
          <span/>
        <//>
      `

    if ( ! R.isNil( children ) )
      return children

    return null
  }

//

const NasaAudioItem = ({ item_details, on_click }) =>
  html`
    <nasa-audio-item
      onClick=${ (  ) => on_click( item_details ) } >

      <span>
        ♪
      <//>
    <//>
  `

const NasaImageItem = ({ item_details, on_click }) =>
  {
    const [ image, title ] = useMemo( (  ) =>
      {
        if( R.isNil( item_details ) )
          return [  ]

        const thumbnail_image = item_details &&
          item_details.find( image_url => SEARCH_IMAGE_THUMBNAIL.test( image_url ) )

        const thumbnail_title = thumbnail_image &&
          NasaTitleGenerate( thumbnail_image )

        return [ thumbnail_image, thumbnail_title ]
      }
    , [ item_details ]
    )

    return html`
      <nasa-image-item>
        <img
          src=${ image }
          alt=${ title }
          onClick=${ (  ) => on_click( item_details )
          } />

      <//>
    `
  }

const NasaListItem = ({ item, type, on_click }) =>
  {
    const [ state, set_state ] = useState( PENDING )

    const [ item_details, set_item_details ] = useState( null )

    useEffect( (  ) => {
      set_state( PENDING )

      ;(async (  ) => {
        if ( R.isNil( item ) )
          return // Create exception

        try {
          const response = await fetch( item )
          
          set_item_details( await response.json(  ) )
        } catch ( e ) {
          console.warn( e )
        } finally {
          set_state( COMPLETE )
        }
      } ) (  )
    }, [ item ] )

    const NasaItemType = useMemo( (  ) =>
      ( { [ NASA_AUDIO_TYPE ]: NasaAudioItem
        , [ NASA_IMAGE_TYPE ]: NasaImageItem
        } [ type ]
      )
    , [ type ] )

    const loading = ( state === PENDING )

    return html`
      <nasa-list-item>
        <${ StateLoaderPending }
          pending=${ loading }>

          <${ NasaItemType }
            item_details=${ item_details }
            on_click=${ on_click } />

        <//>
      <//>
    `
  }

const NasaList = ({ on_select }) =>
  {
    const [ options, set_options ] = useState({ audio: true, image: true })

    const [ results, set_results ] = useState( null )

    const [ query, set_query ] = usePersistedState( 'nasa-search-query' )

    useDebouncedEffect( (  ) => {
      if ( R.isNil( query ) )
        return

      if ( R.isEmpty( query ) ) {
        set_results( null )

        return
      }

      const media_types =
        R.pipe
          ( R.toPairs // Key value tuples
          , R.map( ([ key, value ]) => value ? key : null )
          , R.reject( R.isNil ) // Remove false / null values
          , R.join( ',' ) // Comma seperated values
          , R.pair( 'media_type' ) // Key value tuple
          ) ( options )

      const search_query =
        R.pipe
          ( R.pair([ 'q', query ]) // Array of tuples
          , R.reject( R.o( R.isEmpty, R.last ) ) // Remove any tuples with empty values
          , query => new URLSearchParams( query ) // Create Search string
          , R.invoker( 0, 'toString' )
          , R.replace( /%2C/g, ',' ) // String value and include commas
          ) ( media_types )

      ;(async (  ) => {
        const response =
          await fetch( `https://images-api.nasa.gov/search?${ search_query }` )
        
        const data = await response.json(  )

        try {
          const { collection: { items, links, metadata: { total_hits } } } = data

          set_results({ items, links, total_hits })
        } catch ( e ) {
          console.warn( e )
        }
      } ) (  )

    }, 500, [ query, options ] )

    return html`
      <nasa-list>
        <header>
          <h1>
            NASA Search
          <//>
        <//>

        <input
          type=text
          placeholder=Search...
          value=${ query }
          onInput=${ ({ target: { value } }) => set_query( value ) } />

        <label>
          <input
            ...${ ! options.audio ? { disabled: true } : {  } }
            type=checkbox
            name=image
            checked=${ options.image }
            onChange=${ ({ target: { checked } }) =>
              set_options({ ...options, image: checked })
            } />
          
          Include Images
        <//>

        <label>
          <input
            ...${ ! options.image ? { disabled: true } : {  } }
            type=checkbox
            name=audio
            checked=${ options.audio }
            onChange=${ ({ target: { checked } }) =>
              set_options({ ...options, audio: checked })
            } />
          
          Include Audio
        <//>

        <hr />

        ${R.isNil( results ) || R.isEmpty( results )
            ? html`
                <h2>
                  Start Typing in the Text Box Above to Seach the NASA Image Library
                <//>`
            : html`
                <${ Fragment }> 
                  <h4>
                    ${ results.total_hits } Results
                  <//>

                  ${ ! R.isNil( results ) && results.items.map( item =>
                      html`<${ NasaListItem }
                              item=${ item.href }
                              type=${ item.data[ 0 ].media_type }
                              on_click=${ urls => on_select({ item, urls, type: item.data[ 0 ].media_type }) } />`
                    )
                  }
                <//>
              `
        }
      <//>
    `
  }

export const NasaItemView = ({ id, urls, item, type }) =>
  {
    const [ show_more, set_show_more ] = useState( false )

    const url = useMemo( (  ) =>
      ( { [ NASA_IMAGE_TYPE ]: (  ) =>
            urls.find( image_url => SEARCH_IMAGE_LARGE.test( image_url ) ) ||
            urls.find( image_url => SEARCH_IMAGE_MEDIUM.test( image_url ) ) ||
            urls.find( image_url => SEARCH_IMAGE_SMALL.test( image_url ) ) ||
            urls.find( image_url => SEARCH_IMAGE_THUMBNAIL.test( image_url ) )
        , [ NASA_AUDIO_TYPE ]: (  ) =>
            urls.find( audio_url => SEARCH_AUDIO_HIGH.test( audio_url ) ) ||
            urls.find( audio_url => SEARCH_AUDIO_MEDIUM.test( audio_url ) ) ||
            urls.find( audio_url => SEARCH_AUDIO_LOW.test( audio_url ) )
        } [ type ]
      ) (  )
    , [ urls, type ] )

    console.log( url, type, urls )

    return html`
      <nasa-item-view>
        <button
          link
          type=button
          onClick=${ (  ) =>
            window.history.back(-1)
          } >
          
        🠴 Back
        <//>

        <header>
          ${ id }
        <//>

        <p
          ...${ show_more ? { open: true } : {  } } >

          ${ item.data[ 0 ].description }
        <//>

        <button
          link
          type=button
          onClick=${ (  ) => set_show_more( ! show_more ) } >
          
          ${ show_more ? 'Show Less' : 'Show More' }
        <//>

        <hr />

        ${type === NASA_IMAGE_TYPE &&
          html`<img src=${ url } alt=${ id } />`
        }

        ${type === NASA_AUDIO_TYPE &&
          html`
            <audio
              controls
              src=${ url } >

                Your browser does not support the
                <code>audio</code> element.
            <//>
          `
        }
      <//>
    `
  }

export const NasaBrowser = (  ) =>
  {
    const [ selected_image, set_selected_image ] = useState( null )

    useEffect( (  ) => {
      if( R.isNil( selected_image ) )
        return

      const { urls } = selected_image

      const title = NasaTitleGenerate( urls[ 0 ] ) 

      route( `/asset/${ title }` )
    }, [ selected_image ] )

    const { item, urls, type } = ( selected_image || [  ] )

    return html`
      <nasa-browser>
        <${ Router }>
          <${ NasaItemView }
            path=/asset/:id
            item=${ item }
            urls=${ urls }
            type=${ type } />

          <${ NasaList } default on_select=${ set_selected_image } />
          <${ ErrorPage } type=404 default />
        <//>
      <//>
    `
  }
