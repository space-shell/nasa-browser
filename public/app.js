import 'preact/devtools'

import { html } from 'htm/preact'

import { render } from 'preact'

import { NasaBrowser } from './components/nasa-browser.js'

window.customElements.define
  ( 'app-main'
  , class extends HTMLElement {
      constructor(  ) {
        super(  )
      }

      connectedCallback(  ) {
        render
          ( html`<${ NasaBrowser } />`
          , this
          )
      }
    }
  )
